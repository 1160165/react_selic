import axios from 'axios'

export const MEDIA_ANUAL_STARTED = 'MEDIA_ANUAL_STARTED'
export const MEDIA_ANUAL_SUCCESS = 'MEDIA_ANUAL_SUCCESS'
export const MEDIA_ANUAL_FAILURE = 'MEDIA_ANUAL_FAILURE'

export const actionMediaAnual = (ano) => {
    return dispatch => {
        dispatch(mediaAnualStarted());
        axios.post("http://localhost:8080/estimativas/media-anual",
            {
                data: {
                    ano: ano
                }
            }
        ).then(res => {
            dispatch(mediaAnualSuccess(res.data));
        })
            .catch(err => {
                dispatch(mediaAnualFailure(err.message));
            });
    };
};

export function mediaAnualStarted() {
    return {
        type: MEDIA_ANUAL_STARTED,
    }
}

export function mediaAnualSuccess(leituras) {
    return {
        type: MEDIA_ANUAL_SUCCESS,
        payload: {
            data: leituras
        }

    }
}

export function mediaAnualFailure(message) {
    return {
        type: MEDIA_ANUAL_FAILURE,
        payload: {
            error: message
        }
    }
}


