import axios from 'axios'

export const TAXA_MENSAL_STARTED = 'TAXA_MENSAL_STARTED'
export const TAXA_MENSAL_SUCCESS = 'TAXA_MENSAL_SUCCESS'
export const TAXA_MENSAL_FAILURE = 'TAXA_MENSAL_FAILURE'

export const actionTaxaMensal = (ano,mes) => {
    return dispatch => {
        dispatch(taxaMensalStarted());
        axios.post("http://localhost:8080/estimativas/taxa-mensal",
            {
                data: {
                    ano: ano,
                    mes: mes
                }
            }
        ).then(res => {
            dispatch(taxaMensalSuccess(res.data));
        })
            .catch(err => {
                dispatch(taxaMensalFailure(err.message));
            });
    };
};

export function taxaMensalStarted() {
    return {
        type: TAXA_MENSAL_STARTED,
    }
}

export function taxaMensalSuccess(leituras) {
    return {
        type: TAXA_MENSAL_SUCCESS,
        payload: {
            data: [...leituras]
        }

    }
}

export function taxaMensalFailure(message) {
    return {
        type: TAXA_MENSAL_FAILURE,
        payload: {
            error: message
        }
    }
}


