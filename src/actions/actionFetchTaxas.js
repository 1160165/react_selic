import axios from 'axios'

export const FETCH_TAXAS_STARTED = 'FETCH_TAXAS_STARTED'
export const FETCH_TAXAS_SUCCESS = 'FETCH_TAXAS_SUCCESS'
export const FETCH_TAXAS_FAILURE = 'FETCH_TAXAS_FAILURE'

export const actionFetchTaxas = () => {
    return dispatch => {
        dispatch(fetchTaxasStarted());
        axios.get("http://localhost:8080/estimativas/taxa-mensal",
        ).then(res => {
            dispatch(fetchTaxasSuccess(res.data));
        })
            .catch(err => {
                dispatch(fetchTaxasFailure(err.message));
            });
    };
};

export function fetchTaxasStarted() {
    return {
        type: FETCH_TAXAS_STARTED,
    }
}

export function fetchTaxasSuccess(leituras) {
    return {
        type: FETCH_TAXAS_SUCCESS,
        payload: {
            data: [...leituras]
        }

    }
}

export function fetchTaxasFailure(message) {
    return {
        type: FETCH_TAXAS_FAILURE,
        payload: {
            error: message
        }
    }
}


