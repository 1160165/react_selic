import axios from 'axios'

export const IMPORTAR_DADOS_STARTED = 'IMPORTAR_DADOS_STARTED'
export const IMPORTAR_DADOS_SUCCESS = 'IMPORTAR_DADOS_SUCCESS'
export const IMPORTAR_DADOS_FAILURE = 'IMPORTAR_DADOS_FAILURE'

export const actionImportarDados = (path) => {
    return dispatch => {
        dispatch(importarDadosStarted());
        axios.post("http://localhost:8080/estimativas/importar",
            {
                path: path
            }
        ).then(res => {
            dispatch(importarDadosSuccess(res.data));
        })
            .catch(err => {
                dispatch(importarDadosFailure(err.message));
            });
    };
};

export function importarDadosStarted() {
    return {
        type: IMPORTAR_DADOS_STARTED,
    }
}

export function importarDadosSuccess(leituras) {
    return {
        type: IMPORTAR_DADOS_SUCCESS,
        payload: {
            data: [...leituras]
        }

    }
}

export function importarDadosFailure(message) {
    return {
        type: IMPORTAR_DADOS_FAILURE,
        payload: {
            error: message
        }
    }
}


