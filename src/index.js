import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { Provider } from 'react-redux';
import { createStore,combineReducers, compose, applyMiddleware} from 'redux'
import thunk from 'redux-thunk';
import {BrowserRouter, Route} from 'react-router-dom'
import reducer from "./reducer/reducer";


const rootReducer = combineReducers({
    reducer:reducer
})

const store = createStore(rootReducer,compose(applyMiddleware(thunk)));

ReactDOM.render(<Provider store={store}>
    <BrowserRouter>
        <Route exact path="/" component={App} />
    </BrowserRouter>
</Provider>, document.getElementById('root'));
