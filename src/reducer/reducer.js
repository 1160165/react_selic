import {MEDIA_ANUAL_STARTED, MEDIA_ANUAL_SUCCESS, MEDIA_ANUAL_FAILURE} from "../actions/actionMediaAnual";
import {TAXA_MENSAL_STARTED, TAXA_MENSAL_SUCCESS, TAXA_MENSAL_FAILURE} from "../actions/actionTaxaMensal";
import {IMPORTAR_DADOS_STARTED, IMPORTAR_DADOS_SUCCESS, IMPORTAR_DADOS_FAILURE} from "../actions/actionImportarDados";
import {FETCH_TAXAS_STARTED, FETCH_TAXAS_SUCCESS, FETCH_TAXAS_FAILURE} from "../actions/actionFetchTaxas";


const initState = {
    loading: false,
    error: null,
    data: [],

    mediaanual: {
        loading: false,
        error: null,
        data: [],
    },
    taxamensal: {
        loading: false,
        error: null,
        data: [],
    },
    taxageral: {
        loading: false,
        error: null,
        data: [],
    }
}

export default function reducer(state = initState, action) {
    switch (action.type) {
        case MEDIA_ANUAL_STARTED:
            return {
                ...state,
                mediaanual: {
                    loading: true,
                    error: null,
                    data: [],
                }
            }
        case MEDIA_ANUAL_SUCCESS:
            return {
                ...state,
                mediaanual: {
                    loading: false,
                    error: null,
                    data: action.payload.data,
                }
            }
        case MEDIA_ANUAL_FAILURE:
            return {
                ...state,
                mediaanual: {
                    loading: false,
                    error: action.payload.error,
                    data: [],
                }
            }
        case TAXA_MENSAL_STARTED:
            return {
                ...state,
                taxamensal: {
                    loading: true,
                    error: null,
                    data: [],
                }
            }
        case TAXA_MENSAL_SUCCESS:
            return {
                ...state,
                taxamensal: {
                    loading: false,
                    error: null,
                    data: [...action.payload.data]
                }
            }
        case TAXA_MENSAL_FAILURE:
            return {
                ...state,
                taxamensal: {
                    loading: false,
                    error: action.payload.error,
                    data: [],
                }
            }
        case IMPORTAR_DADOS_STARTED:
            return {
                ...state,
                loading: true,
                error: null,
                data: [],
            }
        case IMPORTAR_DADOS_SUCCESS:
            return {
                ...state,
                loading: false,
                error: null,
                data: [...action.payload.data]
            }
        case IMPORTAR_DADOS_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload.error,
                data: [],
            }
        case FETCH_TAXAS_STARTED:
            return {
                ...state,
                taxageral: {
                    loading: true,
                    error: null,
                    data: [],
                }
            }
        case FETCH_TAXAS_SUCCESS:
            return {
                ...state,
                taxageral: {
                    loading: false,
                    error: null,
                    data: [...action.payload.data]
                }
            }
        case FETCH_TAXAS_FAILURE:
            return {
                ...state,
                taxageral: {
                    loading: false,
                    error: action.payload.error,
                    data: [],
                }
            }
        default:
            return state
    }

}

