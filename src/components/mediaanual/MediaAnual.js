import React, {Component} from 'react'
import {connect} from "react-redux";
import {actionMediaAnual} from "../../actions/actionMediaAnual";
import Botao from "./Botao";


class MediaAnual extends Component {


    constructor(props) {
        super(props);
        this.state = {
            ano: "",
        };
    }



    updateInputData = input => {
        this.setState({
            ano: input
        });
    };


    handleClick = () => {
        if (this.state.ano.trim()) {
            this.props.onSubmit(this.state.ano);
        } else {
            alert('Error: no data');
        }
    }

    render() {
        return (
            <div className={"MediaAnualDiv"}>
                <p>Insira o ano desejado</p>
                <form onSubmit={e => {
                    e.preventDefault();
                }}>
                <input type="number" onChange={(e) => this.updateInputData(e.target.value)}
                       value={this.state.ano}/>
                <Botao handleClick={this.handleClick} data={this.props.mediaanual.data} ano = {this.state.ano}/>
                </form>
            </div>
        )
    }
}

const
    mapStateToProps = (state) => {
        return {
            mediaanual: {
                loading: state.reducer.mediaanual.loading,
                data: state.reducer.mediaanual.data,
                error: state.reducer.mediaanual.error,
            }
        }
    }

const
    mapDispatchToProps = (dispatch) => {
        return {
            onSubmit: (ano) => {
                dispatch(actionMediaAnual(ano))
            },
        }
    }


export default connect(mapStateToProps, mapDispatchToProps)(MediaAnual)