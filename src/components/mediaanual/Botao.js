import React, {Component} from 'react'

class Botao extends Component {


    render() {
        const {data} = this.props
        return (
            <div>
                <button className="Button101" onClick={this.props.handleClick}> Média Anual</button>
                {data &&
                <div>
                    <p>A média anual da taxa Selic no ano {this.props.ano} é de:</p>
                    <h5>{data}</h5>
                </div>}
            </div>
        )
    }
}

export default Botao