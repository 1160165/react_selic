import React, {Component} from 'react'

class Botao extends Component {


    render() {
        const {data} = this.props
        return (
            <div>
                <button className="Button101" onClick={this.props.handleClick}> Import</button>
                {data &&
                <div>
                    <p>Estimativa da taxa selic:</p>
                    {!this.props.isHidden && data.length===0 &&
                        <h5>Nenhum valor encontrado nesse periodo</h5>}
                    {data && data.map((number, i) =>
                        <h5 key={i}> Data: {number.data_referencia} // Valor da Taxa: {number.estimativa_taxa_selic}</h5>)}
                </div>}
            </div>
        )
    }
}

export default Botao