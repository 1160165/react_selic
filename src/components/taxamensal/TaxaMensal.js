import React, {Component} from 'react'
import {connect} from "react-redux";
import {actionTaxaMensal} from "../../actions/actionTaxaMensal";
import Botao from "./Botao";
import {actionFetchTaxas} from "../../actions/actionFetchTaxas";


class TaxaMensal extends Component {

    componentDidMount() {
        this.props.onFetch();
    }


    constructor(props) {
        super(props);
        this.state = {
            ano: "",
            mes:"",
            isHidden: true,
        };
    }


    updateInputAno = input => {
        this.setState({
            ano: input
        });
    };


    updateInputMes = input => {
        this.setState({
            mes: input
        });
    };

    handleClick = () => {

        if (this.state.ano.trim() && this.state.ano.trim()) {
            this.props.onSubmit(this.state.ano, this.state.mes);
            this.setState({
                isHidden: false});
            this.setState({ano:"",mes:""})
        } else {
            alert('Error: no data');
        }
    }

    render() {
        console.log(this.props.taxamensal.data)

        return (
            <div className={"TaxaMensalDiv"}>
                <p>Insira o ano desejado</p>
                <form onSubmit={e => {
                    e.preventDefault();
                }}>
                    <input type="number" required="required" onChange={(e) => this.updateInputAno(e.target.value)}
                           value={this.state.ano}/>
                    <p>Insira o mês desejado</p>
                    <input type="number" onChange={(e) => this.updateInputMes(e.target.value)}
                           value={this.state.mes}/>
                    <Botao handleClick={this.handleClick} data={this.props.taxamensal.data} ano = {this.state.ano} isHidden = {this.state.isHidden}/>
                    {this.state.isHidden && this.props.taxageral.data && this.props.taxageral.data.map((number, i) =>
                        <h5 key={i}> Data: {number.data_referencia} // Valor da Taxa: {number.estimativa_taxa_selic}</h5>)}
                </form>
            </div>
        )
    }
}

const
    mapStateToProps = (state) => {
        return {
            taxageral: {
                loading: state.reducer.taxageral.loading,
                data: state.reducer.taxageral.data,
                error: state.reducer.taxageral.error,
            },
            taxamensal: {
                loading: state.reducer.taxamensal.loading,
                data: state.reducer.taxamensal.data,
                error: state.reducer.taxamensal.error,
            }
        }
    }

const
    mapDispatchToProps = (dispatch) => {
        return {
            onFetch: () => {
                dispatch(actionFetchTaxas())
            },
            onSubmit: (ano,mes) => {
                dispatch(actionTaxaMensal(ano,mes))
            },
        }
    }


export default connect(mapStateToProps, mapDispatchToProps)(TaxaMensal)