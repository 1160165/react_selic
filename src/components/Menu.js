import React, {Component} from 'react'

import {Button, ButtonGroup} from 'reactstrap';
import {Link} from "react-router-dom";

class Menu extends Component {


    render() {
        return (
            <div className={'Menu'}>
                <ButtonGroup>
                    <Link to="/estimativas/importar">
                        <Button className={'button2'}>
                            Escolha o ficheiro com o banco de dados das taxas selic
                        </Button>
                    </Link>
                    <Link to="/estimativas/media-anual">
                        <Button className={'button2'}>
                            Media anual da taxa Selic
                        </Button>
                    </Link>
                    <Link to="/estimativas/taxa-mensal">
                        <Button className={'button2'}>
                            Taxa Mensal Selic
                        </Button>
                    </Link>
                </ButtonGroup>
            </div>
        )
    }
}
export default Menu