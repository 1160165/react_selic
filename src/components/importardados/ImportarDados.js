import React, {Component} from 'react'
import {connect} from "react-redux";
import {actionImportarDados} from "../../actions/actionImportarDados";
import FileImg from "../../imgs/ficheiro.png";

class ImportarDados extends Component {


    constructor(props) {
        super(props);
        this.state = {
            path: "",
        };
    }


    updateInputPath = input => {
        this.setState({
            path: input
        });
    };


    handleClick = () => {
        console.log(this.state)
        if (this.state.path.trim()) {
            this.props.onSubmit(this.state.path);
        } else {
            alert('Error: no data');
        }
    }

    render() {
        return (
            <div className={"ImportarDiv"}>
                <p>Insira o caminho para o ficheiro Json <img className={"File"} src={FileImg} alt='File'/></p>
                <form onSubmit={e => {
                    e.preventDefault();
                }}>
                    <input onChange={(e) => this.updateInputPath(e.target.value)}
                           value={this.state.path}/>
                    <button className="Button260" onClick={this.handleClick}> Importar Dados</button>
                    {this.props.data &&
                    <h5>{this.props.data}</h5>}
                </form>
            </div>
        )
    }
}

const
    mapStateToProps = (state) => {
        return {
            loading: state.reducer.loading,
            data: state.reducer.data,
            error: state.reducer.error,
        }
    }

const
    mapDispatchToProps = (dispatch) => {
        return {
            onSubmit: (path) => {
                dispatch(actionImportarDados(path))
            },
        }
    }


export default connect(mapStateToProps, mapDispatchToProps)(ImportarDados)