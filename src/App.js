import React from 'react';
import './App.css';
import {BrowserRouter, Route} from 'react-router-dom'
import MediaAnual from "./components/mediaanual/MediaAnual";
import Menu from "./components/Menu";
import TaxaMensal from "./components/taxamensal/TaxaMensal";
import ImportarDados from "./components/importardados/ImportarDados";

function App() {
    return (

            <BrowserRouter>
                <div>
                    <Route exact path='/' component={Menu}/>
                    <Route exact path='/estimativas/importar' component={ImportarDados}/>
                    <Route exact path='/estimativas/media-anual' component={MediaAnual}/>
                    <Route exact path='/estimativas/taxa-mensal' component={TaxaMensal}/>
                </div>
            </BrowserRouter>

    );
}

export default App;
